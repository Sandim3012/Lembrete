/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jera.web;


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sandim
 */
@WebServlet(urlPatterns="/novolembrete")
public class NovoLembrete extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {
        /*Recebe o ID do livro selecionado.
          Transforma ele em uma String para poder relacionar com o lembrete*/
        String id = req.getParameter("id");
        //
        
        PrintWriter saida = resp.getWriter();
        
        /*Recebe hora e data.
          Envia o ID do livro selecionado.
          */
        saida.println("<html>" 
                    + "     <body>" 
                    + "         <form "
                    + "             action='lembrete' "
                    + "             method='POST'>" 
                    + "         <h1>"
                    + "             Informe data e hora do lembrete:"
                    + "         </h1>" 
                    + "         <br>" 
                    + "         <input "
                    + "             type='hidden' "
                    + "             value='"+id+"' "
                    + "             name='id'>"
                    + "         <input "
                    + "             type='date' "
                    + "             name='data'>" 
                    + "         <input "
                    + "             type='submit' "
                    + "             value='Confirmar'>" 
                    + "         </form>" 
                    + "     </body>" 
                    + "</html>");
    }
    
}
