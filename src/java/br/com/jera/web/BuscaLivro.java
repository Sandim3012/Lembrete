/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jera.web;

import br.com.jera.dao.LivroDAO;
import br.com.jera.gerenciador.Livro;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sandim
 */
@WebServlet(urlPatterns="/buscalivro")
public class BuscaLivro extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter saida = resp.getWriter();
        Collection<Livro> livros = new LivroDAO().buscaLivro();
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy:hh;mm");
        Date data = new Date();
        
        saida.println("<html>");
        saida.println("     <body>");
        saida.println("         <form "
                    + "             action='novolembrete' "
                    + "             method='POST'>"
                    + "             <table border='1px'>");
        for(Livro livro : livros )
        {
            saida.println("<tr>"
                        + "         <td>"
                                        + livro.getNome()+
                          "        </td>"
                        + "         <td>"
                                        +livro.getPaginas()+
                          "        </td>"
                        + "         <td>"
                        + "         <input "
                        + "             type='hidden' "
                        +               "value='"+livro.getId()+"' "
                        + "             name='id'>"
                        + "         <input "
                        + "             type='submit' "
                        + "             width='5px' height='5px' "
                        + "             value='Adicionar Lembrete'>"
                        + "         </td>"
                        + "  </tr>");
        }
        saida.println("             </table>");
        saida.println("     </body>");
        saida.println("</html>");
    }

  
}
