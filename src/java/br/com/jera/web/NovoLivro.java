/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jera.web;

import br.com.jera.dao.LivroDAO;
import br.com.jera.gerenciador.Livro;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sandim
 */
@WebServlet(urlPatterns="/novolivro")
public class NovoLivro extends HttpServlet {
    
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
    {
        
        /*Recebe os dados: Nome, paginas.
          Insere na lista pelo metodo adicionarLivro*/
        PrintWriter saida = resp.getWriter();
        String nome = req.getParameter("nome");
        int paginas = Integer.valueOf(req.getParameter("paginas"));
        Livro livro = new Livro(nome,paginas);
        LivroDAO livrodao = new LivroDAO();
        livrodao.adicionarLivro(livro);
        //
        
        saida.println("<html>"
                    + "     <body>"
                    + "         Livro cadastrado"
                    + "     </body>"
                    + "</html>");
        
        resp.sendRedirect("paginainicial.html");   
    }
     
    
 
}
