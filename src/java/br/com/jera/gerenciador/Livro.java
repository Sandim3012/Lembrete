/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jera.gerenciador;

import java.util.Date;

/**
 *
 * @author Sandim
 */
public class Livro 
{
    private long id;
    private String nome;
    private int paginas;
    private Date lembrete;

    public Date getLembrete() 
    {
        return lembrete;
    }

    public void setLembrete(Date lembrete) 
    {
        this.lembrete = lembrete;
    }
    
    public Livro()
    {
        
    }
    public long getId() 
    {
        return id;
    }

    public void setId(long id) 
    {
        this.id = id;
    }
    public Livro(String nome, int paginas)
    {
        this.nome = nome;
        this.paginas = paginas;
    }
    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome) 
    {
        this.nome = nome;
    }

    public int getPaginas() 
    {
        return paginas;
    }

    public void setPaginas(int paginas) 
    {
        this.paginas = paginas;
    }
    
    
}
